#!/usr/bin/ruby

# golfed: ruby -ne 'puts $_.chomp.reverse'

ARGF.each_line do |l|
  puts l.chomp.reverse
end
