#!/usr/bin/ruby

def get_palindromes(length)
  return ['1', '0'] if length == 1
  first = 2**(length / 2) - 1
  first.downto(0).map do |n|
    if length.even?
      get_even_palindrome(n, length)
    else
      get_odd_palindromes(n, length)
    end
  end.flatten
end

def get_even_palindrome(n, length)
  half = n.to_s(2).rjust(length / 2, '0')
  half + half.reverse
end

def get_odd_palindromes(n, length)
  half = n.to_s(2).rjust(length / 2, '0')
  [half + '1' + half.reverse, half + '0' + half.reverse]
end

ARGF.each_line do |l|
  puts get_palindromes(l.to_i).join
end

