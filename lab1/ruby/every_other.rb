#!/usr/bin/ruby

# For extra golfing use:
# ruby -ne 'puts $_.chomp.chars.each_slice(2).map(&:first).join'

ARGF.each_line do |l|
  puts l.chomp.chars.each_slice(2).map(&:first).join
end
