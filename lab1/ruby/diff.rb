#!/usr/bin/ruby

def get_counts(word)
  word.chars.reduce(Hash.new(0)) do |memo, c|
    memo[c] += 1
    memo
  end
end

gets # discard the data size, it's not necessary

ARGF.each_line do |l|
  words = l.chomp.split
  first, second = words.map { |w| get_counts(w) }
  diffs = first.merge(second) { |_, a, b| (a-b).abs }
  puts diffs.values.sum
end
