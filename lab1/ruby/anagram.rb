#!/usr/bin/ruby

gets # discard input size, not necessary

ARGF.each_line do |l|
  first, second = l.chomp.split.map(&:chars).map(&:sort)
  puts first == second ? 'TAK' : 'NIE'
end
