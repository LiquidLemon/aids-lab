#include <cstdio>

#define BUFFER_SIZE 100000

int abs(int x) {
  return x < 0 ? -x : x;
}

void buildHistogram(char const *string, int *histogram) {
  while (*string) {
    int index = *string - 'a';
    histogram[index] += 1;
    string++;
  }

}

int getDiff(int *first, int *second) {
  int diff = 0;

  for (int i = 0; i < 26; i++) {
    diff += abs(first[i]-second[i]);
  }

  return diff;
}

int main() {
  char first[BUFFER_SIZE];
  char second[BUFFER_SIZE];

  int count;
  scanf("%d", &count);

  for (int i = 0; i < count; i++) {
    scanf(" %s", first);
    scanf(" %s", second);
    int firstHistogram[26] = { 0 };
    int secondHistogram[26] = { 0 };

    buildHistogram(first, firstHistogram);
    buildHistogram(second, secondHistogram);

    printf("%d\n", getDiff(firstHistogram, secondHistogram));
  }

  return 0;
}
