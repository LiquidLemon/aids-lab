#include <cstdio>

#include <cstring>

#define BUFFER_SIZE 100000

int getDifference(char *first, char *second) {
  if (*first && *second) {
    int diff = 0;
    char *greater;
    char *smaller;
    if (*first > *second) {
      greater = first;
      smaller = second;
    } else {
      greater = second;
      smaller = first;
    }

    while (*smaller && *greater && *smaller < *greater) {
      diff++;
      smaller++;
    }

    while (*smaller && *greater && *smaller == *greater) {
      smaller++;
      greater++;
    }

    return diff + getDifference(smaller, greater);
  } else if (*first) {
    return strlen(first);
  } else if (*second) {
    return strlen(second);
  } else {
    return 0;
  }
}

int main() {
  char first[BUFFER_SIZE];
  char second[BUFFER_SIZE];

  while (scanf(" %s", first) == 1 && scanf(" %s", second) == 1) {
    printf("%d\n", getDifference(first, second));
  }

  return 0;
}
