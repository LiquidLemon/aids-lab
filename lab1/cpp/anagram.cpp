#include <cstdio>
#define BUFFER_SIZE 100000

void buildHistogram(char const *string, int *histogram) {
  while (*string) {
    int index = *string - 'a';
    histogram[index] += 1;
    string++;
  }
}

bool compareHistograms(int *first, int *second) {
  for (int i = 0; i < 26; i++) {
    if (first[i] != second[i]) {
      return false;
    }
  }
  return true;
}

int main() {
  char first[BUFFER_SIZE];
  char second[BUFFER_SIZE];

  int count;
  scanf("%d", &count);

  for (int i = 0; i < count; i++) {
    scanf(" %s", first);
    scanf(" %s", second);
    int firstHistogram[26] = { 0 };
    int secondHistogram[26] = { 0 };

    buildHistogram(first, firstHistogram);
    buildHistogram(second, secondHistogram);

    if (compareHistograms(firstHistogram, secondHistogram)) {
      printf("TAK\n");
    }
    else {
      printf("NIE\n");
    }
  }
  return 0;
}
