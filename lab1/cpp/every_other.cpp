#include <cstdio>

#define BUFFER_SIZE 100000
char buffer[BUFFER_SIZE] = {0};

int main()
{
  while (fgets(buffer, BUFFER_SIZE, stdin)) {
    char *character = buffer;
    do {
      putchar(*character);
      character += 2;
    } while (*character && *(character - 1) && *character != '\n' && *(character - 1) != '\n');
    putchar('\n');
  }
  return 0;
}
