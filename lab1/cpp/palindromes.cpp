#include <cstdio>
#include <cmath>
#include <cstring>
#include <cstdlib>

#define BUFFER_SIZE 100000

void getBinary(int number, char *buffer, int length) {
  for (int i = length-1; i >= 0; i--) {
    buffer[i] = number % 2 == 0 ? '0' : '1';
    number /= 2;
  }
}

void printReverse(char *string) {
  int length = strlen(string);
  for (int i = length - 1; i >= 0; i--) {
    putchar(string[i]);
  }
}

void printPalindromes(int number, int length, char* buffer) {
  if (number < 0) {
    return;
  }

  getBinary(number, buffer, length/2);
  if (length % 2 == 0) {
    printf("%s", buffer);
    printReverse(buffer);
  } else {
    if (length == 1) {
      printf("10");
      return;
    } else {
      printf("%s", buffer);
      putchar('1');
      printReverse(buffer);

      printf("%s", buffer);
      putchar('0');
      printReverse(buffer);
    }
  }

  printPalindromes(number - 1, length, buffer);
}

int main() {
  int length;
  while (scanf(" %d", &length) == 1) {
    int first = (1 << (length / 2)) - 1;
    char *buffer = (char*)malloc(sizeof(char) * (length/2) + 1);
    buffer[length/2] = '\0';
    printPalindromes(first, length, buffer);
    free(buffer);
    putchar('\n');
  }
  return 0;
}
