#include <cstdio>

#define BUFFER_SIZE 100000

char buffer[BUFFER_SIZE] = {0};

size_t get_length(char const *string) {
  size_t length = 0;
  while (*string && *string != '\n') {
    length++;
    string++;
  }
  return length;
}

int main() {
  while (fgets(buffer, BUFFER_SIZE, stdin)) {
    size_t length = get_length(buffer);
    for (int i = length - 1; i >= 0; i--) {
      putchar(buffer[i]);
    }
    putchar('\n');
  }
  return 0;
}

