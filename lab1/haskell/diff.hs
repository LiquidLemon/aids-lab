import qualified Data.Map.Strict as Map

main :: IO ()
main = interact $ unlines . map (show . diff) . lines

diff :: String -> Int
diff x = difference (getCounts first) (getCounts second)
    where [first, second] = words x

getCounts :: Ord a => [a] -> (Map.Map a Int)
getCounts [] = Map.empty
getCounts (x:xs) = Map.insertWith (+) x 1 $ getCounts xs

difference :: Ord a => (Map.Map a Int) -> (Map.Map a Int) -> Int
difference x y = sum $ Map.elems $ Map.unionWith diff x y
    where diff x y = abs (x - y)
