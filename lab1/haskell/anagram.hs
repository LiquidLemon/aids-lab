import qualified Data.List as List

main :: IO ()
main = interact $ unlines . map anagram . lines

anagram :: String -> String
anagram x = if first == second then "TAK" else "NIE"
    where [first, second] = map List.sort $ words x

-- Alternate, supposedly more optimal solution using a map
{-
anagram :: String -> String
anagram x = if first == second then "TAK" else "NIE"
    where [first, second] = map getCounts $ words x

getCounts :: Ord a => [a] -> (Map.Map a Int)
getCounts [] = Map.empty
getCounts (x:xs) = Map.insertWith (+) x 1 $ getCounts xs

difference :: Ord a => (Map.Map a Int) -> (Map.Map a Int) -> Int
difference x y = sum $ Map.elems $ Map.unionWith diff x y
    where diff x y = abs (x - y)
-}
