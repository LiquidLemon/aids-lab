main :: IO ()
main = interact $ unlines . map reverse' . lines

-- Using `reverse` would be just too easy
reverse' :: [a] -> [a]
reverse' [] = []
reverse' (x:xs) = reverse' xs ++ [x]
