main :: IO ()
main = interact $ unlines . map everyOther . lines

everyOther :: [a] -> [a]
everyOther (x:_:xss) = x : everyOther xss
everyOther xs = xs
