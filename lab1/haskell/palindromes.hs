main :: IO ()
main = interact $ unlines . map act . lines
  where act = concat . palindromes . read

binary :: Int -> Int -> String
binary _ 0 = ""
binary x len = (binary (x `div` 2) (len - 1)) ++ [digit]
    where digit = head $ show (x `mod` 2)

-- Get all binary palindromes of a given length in descending order
palindromes :: Int -> [String]
palindromes 0 = []
palindromes 1 = ["1", "0"]
palindromes x
    | even x = map (flip evenPalindrome x) [max,max-1..0]
    | otherwise = concat $ map (flip oddPalindrome x) [max,max-1..0]
    where max = 2^(x `div` 2)-1


evenPalindrome :: Int -> Int -> String
evenPalindrome x len = half ++ (reverse half)
    where half = binary x (len `div` 2)

oddPalindrome :: Int -> Int -> [String]
oddPalindrome x len = [half ++ "1" ++ (reverse half), half ++ "0" ++ (reverse half)]
    where half = binary x (len `div` 2)
