# Lab 1

## Every Other

For each line of input print every other character.

### Input

```
a
ab
abc
abcd
1234567
```

### Output

```
a
a
ac
ac
1357
```

## Reverse

For each line of input print it in reverse.

### Input

```
alfa
beta
gamma
```

### Output

```
afla
ateb
ammag
```

## Difference

Each line of input contains two words. Print how many operations (adding or
removing letters) have to be made to transform one into the other.

The first line of input contains the number of following lines.

### Input

```
3
abc bcd
abc def
aac bdd
```

### Output

```
2
6
6
```

## Anagram


Each line of input contains two words. Print `TAK` if they're anagrams or `NIE`
otherwise.

The first line of input contains the number of following lines.

### Input

```
12
a a
b a
abc cba
abc abc
abc bac
abc xyz
aabc abac
aabc abc
abc aabc
aabbcc abcccc
abcdefghijklmnopq qponmlkjihgfedcba
abcxxxxxxxxxxx qpoxxxxxxxxxxx
```

### Output

```
TAK
NIE
TAK
TAK
TAK
NIE
TAK
NIE
NIE
NIE
TAK
NIE
```

## Palindromes

For each line of input print all the binary anagrams of that length in
descending order.

### Input

```
6
1
5
```

### Output

```
111111110011101101100001011110010010001100000000
10
1111111011101011000101110010100010000000
```

---

Disclaimer - Haskell solutions don't follow the exact specifications, they don't
need to have the data size specified (and won't work with it).
